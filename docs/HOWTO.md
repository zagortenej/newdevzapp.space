## howto

### start

```
$ hugo new site my_sitename
$ cd my_sitename
$ hugo new theme my_theme
```

Add

    theme = "simple-theme"

to `config.toml`

Copy `config.toml` to `dev.config.toml` and set `baseUrl` to `""`.

Start hugo server:

```
$ hugo server --config dev.config.toml
```

### content and basic index template

```
$ hugo new _index.md
```

Add this to `themes/my_theme/layouts/index.html`:

```
<!DOCTYPE html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
    <meta charset="utf-8">

    <base href="{{ .Site.BaseURL }}">
    <title>{{ .Site.Title }}</title>
    <link rel="canonical" href="{{ .Permalink }}">
</head>
<body lang="en">

{{ .Content }}

</body>
</html>
```

Refresh browser and you should see page with site title and whatever you have put in `content/_index.md`.

